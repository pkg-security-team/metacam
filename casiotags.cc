/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)
Copyright (C) 2001, Jan Brittenson (bson@rockgarden.net)
Copyright (C) 2001, Patrick Tullmann (pat@tullmann.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include "dpyfuncs.h"

static const char *rcsid __attribute__((unused))="$Id: casiotags.cc,v 1.4 2002/09/02 19:20:01 daniel Exp $";

// Initial Casio tag references from
//   http://www.ba.wakwak.com/~tsuruzoh/Computer/Digicams/exif-e.html
//   and
//   http://www.dicasoft.de/dicainfo.htm

knowntag casio_known[] = {
    { 0x1, tSHORT,	0, "Recording Mode"              , dpyUnsignedLookup, lookCasioRecMode},
    { 0x2, tSHORT,	0, "Quality"                     , dpyUnsignedLookup, lookCasioQuality},
    { 0x3, tSHORT,	0, "Focusing Mode"               , dpyUnsignedLookup, lookCasioFocusMode},
    { 0x4, tSHORT,	0, "Flash Mode"                  , dpyUnsignedLookup, lookCasioFlashMode},
    { 0x5, tSHORT,	0, "Flash Intensity"             , dpyUnsignedLookup, lookCasioFlashInten},
    { 0x6, tLONG,	0, "Object Distance ~"           , dpyCasioDistance, 0},
    { 0x7, tSHORT,	0, "White Balance"               , dpyUnsignedLookup, lookCasioWhiteBalance},
    // 0x8 is unknown tSHORT
    // 0x9 is unknown tSHORT
    { 0xa, tLONG,	0, "Digital Zoom"                , dpyUnsignedLookup, lookCasioDigitalZoom},
    { 0xb, tSHORT,	0, "Sharpness"                   , dpyUnsignedLookup, lookCasioSharpness},
    { 0xc, tSHORT,	0, "Contrast"                    , dpyUnsignedLookup, lookCasioContrast},
    { 0xd, tSHORT,	0, "Saturation"                  , dpyUnsignedLookup, lookCasioSaturation},
    // 0xe through 0x013 are unknown tSHORT
    { 0x14,tSHORT,	0, "CCD Sensitivity"             , dpyUnsignedLookup, lookCasioSensitivity},

    {0,0,0,0,0}
};


