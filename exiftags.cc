/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include "dpyfuncs.h"

static const char *rcsid __attribute__((unused))="$Id: exiftags.cc,v 1.5 2002/09/02 19:20:01 daniel Exp $";

// Many of these from the TIFF V6.0 Standard
// Extra EXIF references from:
//       http://www.butaman.ne.jp/~tsuruzoh/Computer/Digicams/exif-e.html

knowntag main_known[] = {
    {306,     2, 0, "Image Creation Date"         , dpyString, 0},
    {271,     2, 0, "Make"                        , dpyString, 0},
    {272,     2, 0, "Model"                       , dpyString, 0},
    {305,     2, 0, "Software Version"            , dpyString, 0},
    {296,     3, 0, "Resolution Unit"             , dpyResolutionType, 0},
    {282,     5, 0, "X Resolution"                , dpyResolution, 0},
    {283,     5, 0, "Y Resolution"                , dpyResolution, 0},
    {277,     3, 0, "Samples Per Pixel"           , dpyUnsigned, 0},
    {258,     3, 0, "Bits Per Sample"             , dpyBitsPerSample, 0},
    {270,     2, 0, "Image Description"           , dpyString, 0},
    {513,     4, 0, "Jpeg File Format"            , dpyYes, 0},
    {262,     3, 0, "Photometric Interpretation"  , dpyUnsignedLookup, lookPhotometric},
    {259,     3, 0, "Compression"                 , dpyUnsignedLookup, lookCompression},
    {0x0213,  3, 0, "YCbCr Positioning"           , dpyUnsignedLookup, lookYCbCrPositioning},
    {0x0112,  3, 1, "[Orientation]"               , 0, 0},
    {0x013E,  5, 1, "[White Point]"               , 0, 0},
    {0x013F,  5, 1, "[Primary Chromacities]"      , 0, 0},
    {0x0211,  5, 1, "[YCbCr Coefficients]"        , 0, 0},
    {0x0214,  5, 1, "[Reference Black/White]"     , 0, 0},

    {257,     3, 1, "Image Length"                , dpyNULL, 0},
    {257,     4, 1, "Image Length"                , dpyNULL, 0},
    {256,     3, 1, "Image Width"                 , dpyNULL, 0},
    {256,     4, 1, "Image Width"                 , dpyNULL, 0},
    {278,     3, 2, "Rows Per Strip"              , dpyNULL, 0},
    {278,     4, 2, "Rows Per Strip"              , dpyNULL, 0},
    {273,     3, 2, "Strip Offsets"               , dpyNULL, 0},
    {273,     4, 2, "Strip Offsets"               , dpyNULL, 0},
    {279,     3, 2, "Strip Byte Counts"           , dpyNULL, 0},
    {279,     4, 2, "Strip Byte Counts"           , dpyNULL, 0},
    {284,     3, 2, "Planar Configuration"        , dpyNULL, 0},
    {514,     4, 2, "JPEG Interchange Fmt Length" , dpyNULL, 0},
    {515,     3, 2, "JPEG Restart Interval"       , dpyNULL, 0},

    // Sometimes Exif tags appear here too
    {0x9003,  2, 0, "Image Capture Date"          , dpyString, 0},

    {0,0,0,0,0}
};

knowntag exif_known[] = {
    {0x9291,  2, 0, "Sub-Second Creation Time"    , dpyString, 0},
    {0x9003,  2, 0, "Image Capture Date"          , dpyString, 0},
    {0x9290,  2, 0, "Sub-Second Capture Time"     , dpyString, 0},
    {0x9004,  2, 0, "Image Digitized Date"        , dpyString, 0},
    {0x9292,  2, 0, "Sub-Second Digitized Time"   , dpyString, 0},
    {0x9204, 10, 0, "Exposure Bias"               , dpyExpAdjust, 0},
    {0x920a,  5, 0, "Focal Length"                , dpyZoom, 0},
    {0x829a,  5, 0, "Exposure Time"               , dpyShutter, 0},

    {0x829d,  5, 0, "Aperture"                    , dpyAperture, 0},
    {0xa002,  3, 0, "Exif Image Width"            , dpyPixels, 0},
    {0xa002,  4, 0, "Exif Image Width"            , dpyPixels, 0},
    {0xa003,  3, 0, "Exif Image Height"           , dpyPixels, 0},
    {0xa003,  4, 0, "Exif Image Height"           , dpyPixels, 0},

    {0x8822,  3, 0, "Exposure Program"            , dpyUnsignedLookup, lookExposureProgram},
    {0xa402,  3, 0, "Exposure Mode"               , dpyUnsignedLookup, lookExposureMode},
    {0xa403,  3, 0, "White Balance"               , dpyUnsignedLookup, lookWhiteBalance},
    {0x8827,  3, 0, "ISO Speed Rating"            , dpyISO, 0},
    {0x9207,  3, 0, "Metering Mode"               , dpyUnsignedLookup, lookMeteringMode},
    {0x8824,  2, 0, "Spectral Sensitivity"        , dpyString, 0},
    {0x9000,  7, 0, "EXIF Version"                , dpyExifVersion, 0},
    {0xa000,  7, 0, "FlashPix Version"            , dpyExifVersion, 0},
    {0x9208,  3, 0, "Light Source/White Balance"  , dpyUnsignedLookup, lookLightSource},
    {0x9209,  3, 0, "Flash"                       , dpyExifFlash, 0}, // Exif 2.2 p39
    {0xa217,  3, 0, "Sensing Method"              , dpyUnsignedLookup, lookSensingMethod},
    {0x9102,  5, 0, "Compressed Bits Per Pixel"   , dpyRationalAsDouble, 0},
    {0x9202,  5, 0, "Aperture Value"              , dpyExifAperture, 0},
    {0x9203,  10,0, "Brightness Value"            , dpyExpAdjust, 0},
    {0x9205,  5, 0, "Max Aperture Value"          , dpyExifAperture, 0},
    {0xa210,  3, 0, "Focal Plane Resolution Unit" , dpyResolutionType, 0},
    {0xa20e,  5, 0, "Focal Plane X Resolution"    , dpyResolution, 0},
    {0xa20f,  5, 0, "Focal Plane Y Resolution"    , dpyResolution, 0},
    {0x8298,  2, 0, "Copyright"                   , dpyString, 0},
    {0x9201,  10,0, "Shutter Speed Value"         , dpyExifShutter, 0},
    {0xa001,  3, 0, "ColorSpace"                  , dpyUnsignedLookup, lookExifColorSpace},
    {0x9101,  7, 0, "Component Configuration"     , dpyExifComponentConfiguration, 0}, /** See page 28 of exif std */
    {0x9206,  10,0, "Subject Distance"            , dpyRationalDistance, "m"},  // Exif 2.2 p 37
    {0x9206,   5,0, "Subject Distance"            , dpyRationalDistance, "m"},  // Exif 2.2 p 37
    {0x920a,  5, 0, "Focal Length"                , dpyRationalDistance, "mm"}, // Exif 2.2 p42
    {0x920a, 10, 0, "Focal Length"                , dpyRationalDistance, "mm"}, // Exif 2.2 p42
    {0xa20b,  5, 0, "Flash Energy"                , dpyRationalAsDouble, "BCPS"}, // Exif 2.2 p42
    {0xa20b, 10, 0, "Flash Energy"                , dpyRationalAsDouble, "BCPS"}, // Exif 2.2 p42
    {0xa215,  5, 0, "Exposure Index"              , dpyRationalAsDouble, 0}, // Exif 2.2 p44
    {0xa215, 10, 0, "Exposure Index"              , dpyRationalAsDouble, 0}, // Exif 2.2 p44
    {0xa404,  5, 0, "Digital Zoom Ratio"          , dpyZoomRatio, 0}, // Exif 2.2 p 47
    {0xa404, 10, 0, "Digital Zoom Ratio"          , dpyZoomRatio, 0}, // Exif 2.2 p 47
    {0xa405,  3, 0, "35mm Focal Length"           , dpy35mmFocal, 0}, // Exif 2.2 p 47
    
    {0xa406,  3, 0, "Scene Capture Type"          , dpyUnsignedLookup, lookSceneCaptureType},
    {0xa407,  3, 0, "Gain Control"                , dpyUnsignedLookup, lookGainControl},
    {0xa408,  3, 0, "Contrast"                    , dpyUnsignedLookup, lookContrast},
    {0xa409,  3, 0, "Saturation"                  , dpyUnsignedLookup, lookSaturation},
    {0xa40a,  3, 0, "Sharpness"                   , dpyUnsignedLookup, lookContrast},
    {0xa40c,  3, 0, "Subject Distance Range"      , dpyUnsignedLookup, lookSubjectDistanceRange},
    {0xa420,  2, 0, "Image Unique ID"             , dpyString, 0},
    {0xa301,  7, 1, "Scene Type"                  , dpyUndefinedLookup, lookSceneType}, // Exif 2.2 p 45
    {0xa300,  7, 1, "File Source"                 , dpyUndefinedLookup, lookFileSource}, // Exif 2.2 p 45
    {0xa401, 3, 1, "Custom Rendered"              , dpyUnsignedLookup, lookCustomRendered}, // Exif 2.2 p 46
    
    {0x9286,  7, 1, "User Comment"                , dpyTypedComment, 0},
    
    // Items from Exif standard
    {0x8828,  7, 1, "[OECF]"                      , 0, 0}, // Exif 2.2 p 35 
    {0x9214,  3, 1, "[Subject Area]"              , 0, 0}, // Exif 2.2 p 40
    {0xa20c,  7, 1, "[SpatialFrequencyResponse]"  , 0, 0}, // Exif 2.2 p 42
    {0xa214,  3, 1, "[Subject Location]"          , 0, 0}, // Exif 2.2 p 44
    {0xa302,  7, 1, "[CFA Pattern]"               , 0, 0}, // Exif 2.2 p 45
    {0xa40b,  7, 1, "[Device Setting Description]", 0, 0}, // Exif 2.2 p 49
    
    {0x9286,  7, 1, "[User Comment]"              , 0, 0},

    {0xa004,  2, 1, "[Related Sound File]"        , 0, 0},
    {0xa005,  4, 2, "[ExifInteroperabilityOffset]", 0, 0},
    

    {0,0,0,0,0}
};

knowntag empty_known[] = {
    {0,0,0,0,0}
};

