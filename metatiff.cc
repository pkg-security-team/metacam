/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include <iostream>

#include "metatiff.h"
#include "datatiff.h"
#include "edittiff.h"

static const char *rcsid __attribute__((unused))="$Id: metatiff.cc,v 1.2 2002/09/02 19:20:01 daniel Exp $";

_ReferenceCounted::~_ReferenceCounted()
{
    // Nothing
}

_IFD::~_IFD()
{
    if (getRefCount()) { cerr << "DEBUG: Deleting _IFD with reference count of " << getRefCount() << endl; }
}

_IFDEntry::~_IFDEntry()
{
    if (getRefCount()) { cerr << "DEBUG: Deleting _IFDEntry with reference count of " << getRefCount() << endl; }
}

int 
_IFDEntry::typeLength(unsigned short type)
{
    switch (type) {
    case tBYTE:      return 1; /* BYTE */
    case tASCII:     return 1; /* ASCII */
    case tSHORT:     return 2; /* SHORT */
    case tLONG:      return 4; /* LONG */
    case tRATIONAL:  return 8; /* RATIONAL */
    case tSBYTE:     return 1; /* SBYTE */
    case tUNDEFINED: return 1; /* UNDEFINED */
    case tSSHORT:    return 2; /* SSHORT */
    case tSLONG:     return 4; /* SLONG */
    case tSRATIONAL: return 8; /* SRATIONAL */
    case tFLOAT:     return 4; /* FLOAT */
    case tDOUBLE:    return 8; /* DOUBLE */
    };
    cerr << "WARNING: Unknown field type " << type << endl;
    return 0;
}

_TIFFDataSource::~_TIFFDataSource()
{
    // Nothing
}

IFD 
TIFFDataSource::getIFD() const 
{
    return src->getIFD();
}

IFD 
TIFFDataSource::getIFD(unsigned long ofs) const 
{
    return src->getIFD(ofs);
}

IFD
IFD::getEditable()
{
    return IFD(new _EditIFD(*this));
}

int _BaseAnnotation::next_avail_type = taFIRST_AVAILABLE;

