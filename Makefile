# -----------------------------------------------------------------------------
# MetaCam - Extract EXIF information from digital camera files, with
# support for Vendor specific blocks.
# Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
# -----------------------------------------------------------------------------
#
# $Id: Makefile,v 1.12 2002/09/02 20:28:46 daniel Exp $

# Some operating-system dependent choices for compile-time flags. Uncomment
# whichever is most appropriate
# -- g++ on linux
OSCXXFLAGS=-ansi -pedantic
# -- Cygwin under Windows 2000
#OSCXXFLAGS=

CXX=g++
INCLUDES=

CXXFLAGS=-Wall $(OSCXXFLAGS) -D_GNU_SOURCE -O2 $(INCLUDES)

LIBOBJS=rationals.o exiftags.o \
     nikontags.o olympustags.o canontags.o casiotags.o dpyfuncs.o \
     metatiff.o filetiff.o tiffdata.o dataifd.o dataifdentry.o editifd.o \
     ocontext.o lookups.o	
EXEOBJS=metacam.o
HDRS=metacam.h dpyfuncs.h metatiff.h filetiff.h datatiff.h edittiff.h \
     odrivers.h

DEPS=$(LIBOBJS:.o=.dep) $(EXEOBJS:.o=.dep)

##### TARGETS ######

default:	metacam

clean:
	-rm -f *.o *~ *-

tags:
	etags *.cc *.h

realclean:
	-rm -f metacam
	-rm -f *.o *~ *-
	-rm -f *.dep
	-rm -f dependencies
	-rm -f TAGS

libmetacam.a:	$(LIBOBJS)
	rm -f libmetacam.a
	ar rsc libmetacam.a- $(LIBOBJS)
	mv -f libmetacam.a- libmetacam.a

metacam:	$(EXEOBJS) libmetacam.a
	$(CXX) $(CXXFLAGS) $(EXEOBJS) -o metacam -lm -L. -lmetacam

# Dependency rules
dependencies:	Makefile $(DEPS)
		cat $(DEPS) > dependencies-
		mv -f dependencies- dependencies

# Each .dep file will hold dependencies for both the .o file, and an identical
# list to determine when the .dep file needs rebuilding. Clever eh?
%.dep: %.cc $(HDRS)
		$(CXX) $(CFLAGS) $(INCS) -M -E -c $*.cc > $*.dep-
		sed -e 's/[.]o:/.dep:/' $*.dep- > $*.dep--
		cat $*.dep-- >> $*.dep-
		rm -f $*.dep--
		mv -f $*.dep- $*.dep 

tar:
	-rm -f files.tar
	tar -cvf files.tar $(LIBOBJS:.o=.cc) $(EXEOBJS:.o=.cc) $(HDRS) Makefile THANKS $(wildcard README.*) LICENSE.TXT BUGS

include dependencies

