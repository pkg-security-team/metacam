/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#ifndef EDITTIFF_H_INCLUDED
#define EDITTIFF_H_INCLUDED

#include "metatiff.h"

class _EditIFD;

class _EditIFD : public _IFD {
private:
    IFD source;
    mutable IFD next;

    vector<IFDEntry> table;

    _EditIFD(const _EditIFD &) { abort(); }
    _EditIFD &operator=(const _EditIFD &) { abort(); return *this; }

public:
    _EditIFD(const IFD &src);
    _EditIFD();
    ~_EditIFD();

    virtual unsigned short entries() const;
    virtual IFD nextIFD() const;
    virtual bool isDynamic() const;

    virtual IFDEntry operator[](int n) const;
    virtual IFDEntry findEntry(unsigned long tag, _IFDEntry *after=0) const;

    virtual TIFFDataSource getSource() const;
    virtual unsigned long getSourceOffset() const;
};

/*
class _EditIFDEntry : public _IFDEntry {
private:
    TIFFDataSource source;

    unsigned long value_count;
    unsigned long offset;
public:
    _EditIFDEntry(const TIFFDataSource &src, long ofs,
		  long t, short typ, long vals);
    virtual ~_EditIFDEntry();

    virtual unsigned long values() const;

    virtual vector<tiffUNSIGNED>   getUVALUE() const;
    virtual vector<tiffSIGNED>     getSVALUE() const;
    virtual vector<tiffRATIONAL>   getRATIONAL() const;
    virtual vector<tiffSRATIONAL>  getSRATIONAL() const;
    virtual vector<string>         getSTRING() const;
    virtual vector<unsigned char>  getOPAQUE() const;

    virtual TIFFDataSource getSource() const;
    virtual unsigned long getSourceOffset() const;
};

*/
#endif /* EDITTIFF_H_INCLUDED */
