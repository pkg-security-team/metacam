Description: Fix crashes on ill-formed Jpeg images.
             The patch adds vector size checking before
             accessing its elements.
Author: Aleksey Kravchenko <rhash.admin@gmail.com>
Bug-Debian: https://bugs.debian.org/779695
Bug-Debian: https://bugs.debian.org/779696
Last-Update: 2019-01-21
diff --git a/dpyfuncs.cc b/dpyfuncs.cc
index 7c4be87..5a3aeed 100644
--- a/dpyfuncs.cc
+++ b/dpyfuncs.cc
@@ -36,6 +36,7 @@ void
 dpyISO(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.size() < 2) return;
     ctx.startBlock(name) << v[1] << " (" << v[0] << ")";
     ctx.endBlock();
 }
@@ -44,6 +45,7 @@ void
 dpyString(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<string> v = e.getSTRING();
+    if (v.empty()) return;
     ctx.startBlock(name) << v[0];
     ctx.endBlock();
 }
@@ -74,6 +76,7 @@ void
 dpyLens(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffRATIONAL> v = e.getRATIONAL();
+    if (v.size() < 4) return;
     v[0] = v[0].normalize();
     v[1] = v[1].normalize();
     v[2] = v[2].normalize();
@@ -105,6 +108,7 @@ void
 dpyZoom(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffRATIONAL> v = e.getRATIONAL();
+    if (v.empty()) return;
     v[0] = v[0].normalize();
 
     ctx.startBlock(name);
@@ -123,6 +127,7 @@ void
 dpyExpAdjust(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffSRATIONAL> v = e.getSRATIONAL();
+    if (v.empty()) return;
     v[0] = v[0].normalize();
 
     ctx.startBlock(name);
@@ -139,6 +144,7 @@ void
 dpyShutter(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffRATIONAL> v = e.getRATIONAL();
+    if (v.empty()) return;
     v[0]=v[0].normalize();
  
     ctx.startBlock(name);
@@ -151,6 +157,7 @@ void
 dpyAperture(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffRATIONAL> v = e.getRATIONAL();
+    if (v.empty()) return;
 
     ctx.startBlock(name) << "f" << (double)v[0];
     ctx.endBlock();
@@ -160,6 +167,7 @@ void
 dpyPixels(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.empty()) return;
 
     ctx.startBlock(name) << v[0] << " pixels";
     ctx.endBlock();
@@ -169,6 +177,7 @@ void
 dpySigned(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffSIGNED> v = e.getSVALUE();
+    if (v.empty()) return;
 
     ctx.startBlock(name);
     if (v[0] > 0) {
@@ -183,6 +192,7 @@ void
 dpyUnsigned(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.empty()) return;
     ctx.startBlock(name) << v[0];
     ctx.endBlock();
 }
@@ -191,6 +201,7 @@ void
 dpyResolution(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffRATIONAL> v = e.getRATIONAL();
+    if (v.empty()) return;
     v[0]=v[0].normalize();
     ctx.startBlock(name);
     displayRational(ctx,v[0]);
@@ -214,16 +225,18 @@ dpyResolutionType(OutputContext &ctx, const char *name, const IFDEntry &e, const
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
     const char *resolution_unit = "???";
-    switch (v[0]) {
-    case 1:
-	resolution_unit = "???";
-	break;
-    case 2:
-	resolution_unit = "Inch";
-	break;
-    case 3:
-	resolution_unit = "Centimeter";
-	break;
+    if (!v.empty()) {
+        switch (v[0]) {
+        case 1:
+            resolution_unit = "???";
+            break;
+        case 2:
+            resolution_unit = "Inch";
+            break;
+        case 3:
+            resolution_unit = "Centimeter";
+            break;
+        }
     }
     ctx.setContextValue("resolutionUnit", resolution_unit);
 }
@@ -261,6 +274,7 @@ const char *findLookup(int key, const void *table) {
 void
 dpyUnsignedLookup(OutputContext &ctx, const char *name, const IFDEntry &e, const void*tbl) {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.empty()) return;
     ctx.startBlock(name);
     const char *val = findLookup((int)v[0], tbl);
     if (val) {
@@ -274,6 +288,7 @@ dpyUnsignedLookup(OutputContext &ctx, const char *name, const IFDEntry &e, const
 void
 dpyUndefinedLookup(OutputContext &ctx, const char *name, const IFDEntry &e, const void*tbl) {
     vector<unsigned char> v = e.getOPAQUE();
+    if (v.empty()) return;
     ctx.startBlock(name);
     const char *val = findLookup((int)v[0], tbl);
     if (val) {
@@ -302,6 +317,7 @@ void
 dpyExifAperture(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffRATIONAL> v = e.getRATIONAL();
+    if (v.empty()) return;
     ctx.startBlock(name);
     double a = v[0];
     double f = pow(M_SQRT2, a);
@@ -316,6 +332,7 @@ void
 dpyExifShutter(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffSRATIONAL> v = e.getSRATIONAL();
+    if (v.empty()) return;
     ctx.startBlock(name);
     double a = v[0];
     if (a > 0.0) {
@@ -335,7 +352,9 @@ dpyRationalAsDouble(OutputContext &ctx, const char *name, const IFDEntry &e, con
 
     if (e.getType() == tSRATIONAL) {
 	vector<tiffSRATIONAL> v = e.getSRATIONAL();
-	if (v[0].getDenominator() == 0) {
+	if (v.empty()) {
+	    ctx.os() << "Unknown";
+	} else if (v[0].getDenominator() == 0) {
 	    ctx.os() << "Infinity";
 	} else {
 	    double a = v[0];
@@ -344,7 +363,9 @@ dpyRationalAsDouble(OutputContext &ctx, const char *name, const IFDEntry &e, con
 	}
     } else {
 	vector<tiffRATIONAL> v = e.getRATIONAL();
-	if (v[0].getDenominator() == 0) {
+	if (v.empty()) {
+	    ctx.os() << "Unknown";
+	} else if (v[0].getDenominator() == 0) {
 	    ctx.os() << "Infinity";
 	} else {
 	    double a = v[0];
@@ -360,6 +381,7 @@ void
 dpyOlymSpecialMode(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.size() < 2) return;
     ctx.startBlock(name);
     switch (v[0]) {
     case 0: ctx.os() << "Normal"; break;
@@ -370,7 +392,7 @@ dpyOlymSpecialMode(OutputContext &ctx, const char *name, const IFDEntry &e, cons
 	ctx.os() << "Unknown (" << v[0] << ")"; break;
     }
     ctx.os() << "; Seq " << v[1];
-    if (v[0] == 3) {
+    if (v[0] == 3 && v.size() >= 3) {
 	switch(v[2]) {
 	case 1: ctx.os() << " Left -> Right"; break;
 	case 2: ctx.os() << " Right -> Left"; break;
@@ -387,6 +409,7 @@ void
 dpyOlymZoom(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffRATIONAL> v = e.getRATIONAL();
+    if (v.empty()) return;
     ctx.startBlock(name);
     double a = v[0];
     if (a == 0.0) {
@@ -414,6 +437,7 @@ void
 dpyCanonBlock1(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.size() < 33) v.resize(33, 0);
 
     try {
 	int n = v[0] / 2;
@@ -542,6 +566,7 @@ dpyCanonBlock1(OutputContext &ctx, const char *name, const IFDEntry &e, const vo
 extern void dpyCanonBlock4(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.size() < 16) v.resize(16, 0);
 
     try {	
 //	int n = v[0] / 2;
@@ -586,6 +611,7 @@ extern void dpyCanonBlock4(OutputContext &ctx, const char *name, const IFDEntry
 extern void dpyCanonImageNumber(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.empty()) return;
 
     try {
 	unsigned long n = v[0];
@@ -607,6 +633,7 @@ extern void dpyCanonImageNumber(OutputContext &ctx, const char *name, const IFDE
 extern void dpyCanonSerialNumber(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.empty()) return;
 
     try {
 	unsigned long n = v[0];
@@ -623,6 +650,7 @@ extern void dpyCanonSerialNumber(OutputContext &ctx, const char *name, const IFD
 extern void dpyCasioDistance(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.empty()) return;
     
     const char* units = "mm";
     double dist = double(v[0]);
@@ -644,8 +672,9 @@ extern void dpyCasioDistance(OutputContext &ctx, const char *name, const IFDEntr
 
 extern void dpyExifFlash(OutputContext &ctx, const char *name, const IFDEntry &e, const void*)
 {
-    ctx.startBlock(name);
     vector<tiffUNSIGNED> v = e.getUVALUE();
+    if (v.empty()) return;
+    ctx.startBlock(name);
     
     if (v[0] & 0x01) {
 	ctx.os() << "Flash Fired; ";
@@ -707,7 +736,7 @@ extern void dpyZoomRatio(OutputContext &ctx, const char *name, const IFDEntry &e
     if (e.getType() == tSRATIONAL) {
 	vector<tiffSRATIONAL> v = e.getSRATIONAL();
 	
-	if (v[0].getNumerator() == 0) {
+	if (v.empty() || v[0].getNumerator() == 0) {
 	    ctx.os() << "None";
 	} else {
 	    displayRational(ctx, v[0]);
@@ -716,7 +745,7 @@ extern void dpyZoomRatio(OutputContext &ctx, const char *name, const IFDEntry &e
     } else {
 	vector<tiffRATIONAL> v = e.getRATIONAL();
 
-	if (v[0].getNumerator() == 0) {
+	if (v.empty() || v[0].getNumerator() == 0) {
 	    ctx.os() << "None";
 	} else {
 	    displayRational(ctx, v[0]);
@@ -734,7 +763,7 @@ extern void dpyRationalDistance(OutputContext &ctx, const char *name, const IFDE
     if (e.getType() == tSRATIONAL) {
 	vector<tiffSRATIONAL> v = e.getSRATIONAL();
 	
-	if (v[0].getNumerator() == 0) {
+	if (v.empty() || v[0].getNumerator() == 0) {
 	    ctx.os() << "Unknown";
 	} else if (v[0].getDenominator() == 0) {
 	    ctx.os() << "Infinity";
@@ -745,7 +774,7 @@ extern void dpyRationalDistance(OutputContext &ctx, const char *name, const IFDE
     } else {
 	vector<tiffRATIONAL> v = e.getRATIONAL();
 
-	if (v[0].getNumerator() == 0) {
+	if (v.empty() || v[0].getNumerator() == 0) {
 	    ctx.os() << "Unknown";
 	} else if (v[0].getDenominator() == 0) {
 	    ctx.os() << "Infinity";
@@ -764,7 +793,7 @@ extern void dpy35mmFocal(OutputContext &ctx, const char *name, const IFDEntry &e
 
     vector<tiffUNSIGNED> v = e.getUVALUE();
 
-    if (v[0] == 0) {
+    if (v.empty() || v[0] == 0) {
 	ctx.os() << "Unknown";
     } else {
 	ctx.os() << v[0] << "mm";
@@ -776,6 +805,7 @@ extern void dpy35mmFocal(OutputContext &ctx, const char *name, const IFDEntry &e
 void
 dpyNikonFocusPosition(OutputContext &ctx, const char *name, const IFDEntry &e, const void*) {
     vector<unsigned char> v = e.getOPAQUE();
+    if (v.size() < 2) return;
     ctx.startBlock(name);
     const char *val = findLookup((int)v[1], lookFocusPosition);
     if (val) {
@@ -842,6 +872,7 @@ void
 dpyNikkorLensInfo(OutputContext &ctx, const char *name, 
 		  const IFDEntry &e, const void*) {
     vector<unsigned char> v = e.getOPAQUE();
+    if (v.size() <= 0x17) return;
     ctx.startBlock(name);
 
     double d;
