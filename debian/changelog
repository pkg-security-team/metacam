metacam (1.2-14) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/copyright: updated packaging copyright years.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 22 Feb 2022 21:25:42 -0300

metacam (1.2-13) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/control: bumped Standards-Version to 4.5.0.
  * debian/copyright: updated packaging copyright years.

  [ Debian Janitor ]
  * Fix day-of-week for changelog entry 1.2-4.
  * Fix field name typos in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 24 Feb 2020 20:17:59 -0300

metacam (1.2-12) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.4.1.
  * debian/copyright:
      - Added packaging rights for Samuel Henrique.
      - Updated packaging copyright years.
  * debian/patches/80_Fix-GCC-hardening-CPPFLAGS.patch: created
    to fix CPPFLAGS to provide GCC hardening.
  * debian/watch: removed a not used search line.

  [ Samuel Henrique ]
  * Add salsa-ci.yml.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 09 Jan 2020 00:42:39 -0300

metacam (1.2-11) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field.
  * debian/copyright:
      - Added rights for Aleksey Kravchenko.
      - Updated packaging copyright years.

  [ Aleksey Kravchenko ]
  * Add Upstream Metadata file.
  * Bump std-version to 4.3.0.
  * d/patches:
    - Support DEB_BUILD_OPTIONS=noopt and honour CPPFLAGS.
    - Fix crashes on ill-formed Jpeg images (Closes: #779695, #779696).
    - Fix crash on reading a long string field (Closes: #779697).
    - Fix crash on certain Canon flash modes (LP: #298580).

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 20 Jan 2019 15:33:04 -0200

metacam (1.2-10) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * Migrated DH level to 11.
  * debian/control: bumped Standards-Version to 4.2.1.
  * debian/copyright:
      - Added rights for Raphaël Hertzog.
      - Updated packaging copyright years.
      - Using a secure copyright format in URI.
  * debian/tests/*: added to perform tests.

  [ Raphaël Hertzog ]
  * debian/control:
      - Changed Vcs-* URLs to salsa.debian.org.
      - Updated team maintainer address to Debian Security Tools
        <team+pkg-security@tracker.debian.org>.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 15 Sep 2018 23:15:26 -0300

metacam (1.2-9) unstable; urgency=medium

  * Bumped DH level to 10.
  * debian/control:
      - Bumped Standards-Version to 3.9.8.
      - Updated the Vcs-* fields to use https instead of http and git.
  * debian/copyright:
      - Grouped all upstream copyright blocks.
      - Updated the packaging copyright years.
  * debian/man/:
      - Renamed to debian/manpage.
      - Using create-man.sh instead of genallman.sh.
  * debian/patches/:
      - Added 30_fix-GCC-warnings.patch to fix some GCC warnings.
      - fix-gcc-4.3: renamed to 20_fix-gcc-4.3.patch.
      - Makefile: renamed to 10_add-GCC-hardening.patch.
  * debian/rules: removed the unnecessary variable DEB_CFLAGS_MAINT_APPEND.
  * debian/watch:
      - Bumped to version 4.
      - Removed the extra source to avoid conflicts with uscan.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 23 Nov 2016 13:53:34 -0200

metacam (1.2-8) unstable; urgency=medium

  * Upload to unstable.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 25 May 2015 22:40:56 -0300

metacam (1.2-7) experimental; urgency=medium

  * debian/control:
      - Added the Homepage field.
      - Fixed the Vcs-* fields.
  * debian/rules:
      - Added the DEB_BUILD_MAINT_OPTIONS variable to fix the issues
        pointed by blhc.
      - Added a target to install the new file debian/upstream.changelog.
  * debian/upstream.changelog: added as a changelog from upstream homepage.
  * debian/watch: added a second way to track new releases.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 29 Oct 2014 15:34:14 -0200

metacam (1.2-6) unstable; urgency=medium

  * New maintainer and co-maintainer. Thanks a lot to  Juan Angulo
      Moreno <juan@apuntale.com>, the last maintainer of this package.
  * Migrations:
      - Converted the manpage to txt2man system. Consequently, created
        the debian/man/ directory, removed debian/metacam.1 file and
        updated the debian/manpages file.
      - Debian format to 3.0.
      - debian/copyright to 1.0 format.
      - debhelper level to 9.
      - Dropped the CDBS system.
      - Patches to quilt system (01_makefile_clean.diff was dropped;
        02_gcc-4.3-fix.diff and 03_metacam_gcc-4.3.diff was merged).
  * debian/clean: added to remove a file forgotten by upstream.
  * debian/control:
      - Added the ${misc:Depends} variable to binary block.
      - Bumped Standards-Version to 3.9.6.
      - Improved the long description.
  * debian/copyright: updated the upstream and packaging data.
  * debian/manpages: added to install the manpage.
  * debian/patches/Makefile: added to provide GCC hardening.
  * debian/rules: added the DEB_CFLAGS_MAINT_APPEND variable to fix
      the issues pointed by blhc.
  * debian/watch: improved.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 07 Oct 2014 18:19:00 -0300

metacam (1.2-5) unstable; urgency=low

  * Update Standards-version 3.7.3.
  * Update Debhelper 6.
  * GCC 4.3 patch. (Closes: #455621)

 -- Juan Angulo Moreno <juan@apuntale.com>  Thu, 06 Mar 2008 22:27:06 -0430

metacam (1.2-4) unstable; urgency=low

  * New maintainer (Closes: #425241)
  * [debian/control] Update description
  * Fixed segmentation fault picture Canon S60 (when it was updated the version
    0.6-1 to 1.2-1 error was not present) (Closes: #279342)
  * [debian/watch] Fix URL file
  * [debian/copyright] Updated (Separation by author and copyrights)

 -- Juan Angulo Moreno <juan@apuntale.com>  Tue, 04 Sep 2007 10:17:04 -0400

metacam (1.2-3) unstable; urgency=low

  * GCC 4.3 patch (Closes: #417428)

  * Orphaning the package.

 -- Jarno Elonen <elonen@debian.org>  Fri, 18 May 2007 12:19:33 +0300

metacam (1.2-2) unstable; urgency=low

  * Added CDBS dependency
  * Added a 'watch' file for uscan

 -- Jarno Elonen <elonen@debian.org>  Sat,  4 Sep 2004 02:45:30 +0300

metacam (1.2-1) unstable; urgency=low

  * New upstream release
  * New maintainer (Closes: #269230, #162468)
  * Rewrote 'rules' with CDBS
  * Updated 'copyright' file to actually contain copyrights
  * Updated man page

 -- Jarno Elonen <elonen@debian.org>  Sat,  4 Sep 2004 01:43:09 +0300

metacam (0.6-1.1) unstable; urgency=low

  * Non Maintainer Upload
  * Fix all g++ 3.x error. g++ 3.x is very strict with types
    (Closes: #134021)

 -- Julien LEMOINE <speedblue@debian.org>  Sun, 16 Mar 2003 16:15:30 +0100

metacam (0.6-1) unstable; urgency=low

  * New upstream release
  * Updated to use debhelper 3

 -- Alp Toker <alp@atoker.com>  Wed, 26 Dec 2001 02:10:37 +0000

metacam (0.5-1) unstable; urgency=low

  * Initial Release.

 -- Alp Toker <alp@atoker.com>  Tue,  4 Sep 2001 20:57:44 +0100
