/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include <iostream>
#include <errno.h>
#include <cstring>
#include <netinet/in.h>

#include "metatiff.h"
#include "datatiff.h"

static const char *rcsid __attribute__((unused))="$Id: tiffdata.cc,v 1.2 2002/09/02 19:20:01 daniel Exp $";

unsigned char
_TIFFDataSource::getUByte()
{
    unsigned char b;
    if (getData((unsigned char *)&b, 1) != 1) return 0;
    return b;
}

unsigned short
_TIFFDataSource::getUShort()
{
    unsigned short s;
    if (getData((unsigned char *)&s, 2) != 2) return 0;
    if (bigEndian()) {return ntohs(s);}
    return s;
}

unsigned long
_TIFFDataSource::getULong()
{
    unsigned long l;
    if (getData((unsigned char *)&l, 4) != 4) return 0;
    if (bigEndian()) {return ntohl(l);}
    return l;
}

signed char
_TIFFDataSource::getSByte()
{
    signed char b;
    if (getData((unsigned char *)&b, 1) != 1) return 0;
    return b;
}

signed short
_TIFFDataSource::getSShort()
{
    signed short s;
    if (getData((unsigned char *)&s, 2) != 2) return 0;
    if (bigEndian()) {return ntohs(s);}
    return s;
}

signed long
_TIFFDataSource::getSLong()
{
    signed long l;
    if (getData((unsigned char *)&l, 4) != 4) return 0;
    if (bigEndian()) {return ntohl(l);}
    return l;
}

IFD
_TIFFDataSource::getIFD()
{
    return IFD(new _DataIFD(TIFFDataSource(this), 0));
}

IFD
_TIFFDataSource::getIFD(unsigned long ofs)
{
    return IFD(new _DataIFD(TIFFDataSource(this), ofs));
}



