/* This file is -*-C++-*-
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------

$Id: odrivers.h,v 1.3 2004/08/21 17:23:24 daniel Exp $

*/

#ifndef ODRIVERS_H_INCLUDED
#define ODRIVERS_H_INCLUDED

#include "metacam.h"
#include <sstream>

class DisplayOutputContext : public OutputContext
{
private:
    ostream &o_stream;
    bool in_block;
public:
    DisplayOutputContext(ostream &o) : o_stream(o), in_block(false) {};
    virtual ~DisplayOutputContext();

    virtual ostream &startBlock(const char *hdr);
    virtual ostream &startBlock(const string &hdr);
    virtual ostream &os(); 
    virtual void endBlock();
};

class DisplayOutputDriver : public OutputDriver
{
private:
    ostream &os;
    OutputContext *current_context;
public:
    DisplayOutputDriver(ostream &o) : os(o), current_context(0) {}
    virtual ~DisplayOutputDriver();

    virtual void startFile(const char *fname);
    virtual OutputContext *startGroup(const char *header);
    virtual OutputContext *startGroup(const string &header);
    virtual void endGroup();
    virtual void endFile();
    virtual void close();
};

class XMLOutputDriver : public OutputDriver
{
private:
    ostream &os;
    OutputContext *current_context;
    bool is_open;
    bool in_file;
public:
    XMLOutputDriver(ostream &o);
    virtual ~XMLOutputDriver();

    virtual void startFile(const char *fname);
    virtual OutputContext *startGroup(const char *header);
    virtual OutputContext *startGroup(const string &header);
    virtual void endGroup();
    virtual void endFile();
    virtual void close();

    void outputRaw(const char *buf ) {os << buf; }
    void outputCooked(const char *);
    void outputLine(const char *buf) { os << buf << endl; }
    void outputAttribute(const char *name, const char *value) {
	os << " " << name << "=\"";
	outputCooked(value);
	os << "\"";
    }

    void outputRaw(const string &buf ) {os << buf; }
    void outputCooked(const string &);
    void outputLine(const string &buf) { os << buf << endl; }
    void outputAttribute(const string &name, const string &value) {
	os << " " << name << "=\"";
	outputCooked(value);
	os << "\"";
    }
    void outputAttribute(const char *name, const string &value) {
	os << " " << name << "=\"";
	outputCooked(value);
	os << "\"";
    }
};

class XMLOutputContext : public OutputContext
{
private:
    XMLOutputDriver &driver;
    ostringstream *str;
    bool in_block;
public:
    XMLOutputContext(XMLOutputDriver &drv) : driver(drv), in_block(false) {};
    virtual ~XMLOutputContext();

    virtual ostream &startBlock(const char *hdr);
    virtual ostream &startBlock(const string &hdr);
    virtual ostream &os(); 
    virtual void endBlock();
};

#endif /* ODRIVERS_H_INCLUDED */
