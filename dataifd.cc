/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include <iostream>

#include "datatiff.h"

static const char *rcsid __attribute__((unused))="$Id: dataifd.cc,v 1.2 2002/09/02 19:20:01 daniel Exp $";

_DataIFD::~_DataIFD()
{
    if (entry_count) {
	for (int i=0; i<entry_count; ++i) table[i]->delRef();
	delete [] table;
    }
    table = 0;
}

_DataIFD::_DataIFD(const TIFFDataSource &src,
		   unsigned long o, unsigned long tagofs)
    : _IFD(), source(src), ofs(o), table(0)
{
    source.seek(ofs);
    entry_count = source.getUShort();
    table = new _DataIFDEntry *[entry_count];

    int i;
    unsigned long expofs = source.tell();
    for (i=0; i<entry_count; ++i) {
	unsigned short tag = source.getUShort() + tagofs;
	unsigned short type = source.getUShort();
	unsigned long values = source.getULong();
	unsigned long offset = 0;

	if ((values * _IFDEntry::typeLength(type)) <= 4) {
	    offset = source.tell();
	} else {
	    offset = source.getULong();
	}

	table[i] = new _DataIFDEntry(source, offset, tag, type, values); 
	table[i]->addRef(); // Stop it from deleting itself later!

	expofs += 12;
	source.seek(expofs);
    }

    next_ifd_ofs = source.getULong();
}

unsigned short 
_DataIFD::entries() const {
    return entry_count;
}

IFD
_DataIFD::nextIFD() const 
{
    if (next_ifd_ofs == 0) return IFD();
    return IFD(new _DataIFD(source, next_ifd_ofs, 0));
}

bool _DataIFD::isDynamic() const {
    return false; 
}

IFDEntry
_DataIFD::operator[](int n)  const
{
    if ((n<0) || (n>=entry_count)) {
	cerr << "_DataIFD Array Bounds Violated" << endl;
	exit(2);
    }
    return IFDEntry(table[n]);
}

IFDEntry
_DataIFD::findEntry(unsigned long tag, _IFDEntry *after)  const
{
    bool ready_to_return = (after == 0);
    for (int i=0; i<entry_count; ++i) {
	if (table[i]->getTag() != tag) continue;
	if (ready_to_return) return IFDEntry(table[i]);
	if (table[i] == after) ready_to_return = true;
    }
    return IFDEntry();
}

TIFFDataSource
_DataIFD::getSource() const
{
    return source;
}

unsigned long
_DataIFD::getSourceOffset() const {
    return ofs;
}
