/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)
Copyright (C) 2001, Jan Brittenson (bson@rockgarden.net)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include "dpyfuncs.h"

static const char *rcsid __attribute__((unused))="$Id: canontags.cc,v 1.4 2002/09/02 19:20:01 daniel Exp $";

// Initial Canon tag references from
//       http://www.butaman.ne.jp/~tsuruzoh/Computer/Digicams/exif-e.html

knowntag canon_known[] = {
    { 0x1, tSHORT,	0, ""                            , dpyCanonBlock1, 0},
    // 0x2 is unknown
    // 0x3 is unknown
    { 0x4, tSHORT,	0, ""                            , dpyCanonBlock4, 0},
    { 0x8, tLONG,	0, "Image Number"                , dpyCanonImageNumber, 0},
    { 0x6, tASCII,	0, "Image Type"                  , dpyString, 0},
    { 0x7, tASCII,	0, "Firmware Version"            , dpyString, 0},
    { 0x9, tASCII,      0, "Owner Name"                  , dpyString, 0},
    { 0xc, tLONG,	0, "Camera Serial Number"        , dpyCanonSerialNumber, 0},
//    { 0xf, tSHORT,	0, ""                            , dpyCanonCustomTags, 0},
    // 0x10 is unknown

    {0,0,0,0,0}
};


