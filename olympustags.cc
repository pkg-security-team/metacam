/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include "dpyfuncs.h"

static const char *rcsid __attribute__((unused))="$Id: olympustags.cc,v 1.5 2002/09/02 19:20:01 daniel Exp $";

// Initial olympus tag references from
//       http://www.butaman.ne.jp/~tsuruzoh/Computer/Digicams/exif-e.html

knowntag olympus_known[] = {
    {0x0200, tLONG,      0, "Special Mode",     dpyOlymSpecialMode, 0},
    {0x0201, tSHORT,     0, "JPEG Quality",     dpyUnsignedLookup, lookOlymJPEGQuality},
    {0x0202, tSHORT,     0, "Macro",            dpyUnsignedLookup, lookYesNo},
    {0x0204, tRATIONAL,  0, "Digital Zoom",     dpyOlymZoom, 0},
    {0x0207, tASCII,     0, "Firmware Version", dpyString, 0},
    {0x0208, tASCII,     1, "Picture Info",     dpyString, 0},
    {0x0209, tUNDEFINED, 0, "Camera ID",        dpyExifVersion, 0},
     
    {0,0,0,0}
};


