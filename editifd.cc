/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include <iostream>

#include "edittiff.h"

static const char *rcsid __attribute__((unused))="$Id: editifd.cc,v 1.3 2004/08/21 17:41:35 daniel Exp $";

_EditIFD::_EditIFD(const IFD &src) : source(src) {
    cerr << "Created new EditIFD()" << endl;
    if (source) {
	for (int i=0; i < source.entries(); ++i) table.push_back(source[i]);
    }
}

_EditIFD::_EditIFD()  {
    // Nothing to do
}

_EditIFD::~_EditIFD()
{
    for (unsigned int i=0; i< table.size(); ++i) table[i] = IFDEntry();
    table.clear();
}

unsigned short
_EditIFD::entries() const
{
    return table.size();
}

IFD
_EditIFD::nextIFD() const
{
    if (next) return next;
    if (source) {
	// Copy source next
	IFD snext = source.nextIFD();
	if (!snext) return next;
	next = IFD(new _EditIFD(snext));
    }
    return next;
}

bool
_EditIFD::isDynamic() const
{
    return true;
}

IFDEntry
_EditIFD::operator[](int n) const
{
    if ((n < 0) || (n >= (int)table.size())) {
	cerr << "_DataIFD Array Bounds Violated" << endl;
	exit(2);
    }
    return table[n];
}

IFDEntry
_EditIFD::findEntry(unsigned long tag, _IFDEntry *after)  const
{
    bool ready_to_return = (after == 0);
    for (unsigned int i=0; i<table.size(); ++i) {
	if (table[i].getTag() != tag) continue;
	if (ready_to_return) return table[i];
	if (table[i] == after) ready_to_return = true;
    }
    return IFDEntry();
}

TIFFDataSource
_EditIFD::getSource() const
{
    return TIFFDataSource();
}
 
unsigned long
_EditIFD::getSourceOffset() const
{
    return 0;
}


