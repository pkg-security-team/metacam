/* This file is -*-C++-*-
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------

$Id: dpyfuncs.h,v 1.9 2004/08/21 17:54:20 daniel Exp $

*/

#ifndef DPYFUNCS_H_INCLUDED
#define DPYFUNCS_H_INCLUDED

#include "metacam.h"

// Display functions

extern void dpyISO(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyString(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyLens(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyZoom(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyExpAdjust(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyShutter(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyAperture(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyPixels(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpySigned(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyUnsigned(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyResolution(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyNULL(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyYes(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyResolutionType(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyBitsPerSample(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyUnsignedLookup(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyUndefinedLookup(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyExifVersion(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyExifAperture(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyExifShutter(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyRationalAsDouble(OutputContext &ctx, const char *, const IFDEntry &e, const void*units);
extern void dpyRationalDistance(OutputContext &ctx, const char *, const IFDEntry &e, const void*units);
extern void dpyExifFlash(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyZoomRatio(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpy35mmFocal(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyExifComponentConfiguration(OutputContext &ctx, const char *, const IFDEntry &e, const void*);

extern void dpyNikonFocusPosition(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyUndefinedString(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyTypedComment(OutputContext &ctx, const char *, const IFDEntry &e, const void*);

extern void dpyNikkorLensInfo(OutputContext &ctx, const char *, const IFDEntry &e, const void*);

extern void dpyOlymSpecialMode(OutputContext &ctx, const char *, const IFDEntry &e, const void*);
extern void dpyOlymZoom(OutputContext &ctx, const char *name, const IFDEntry &e, const void*);

extern void dpyCanonBlock1(OutputContext &ctx, const char *name, const IFDEntry &e, const void*);
extern void dpyCanonBlock4(OutputContext &ctx, const char *name, const IFDEntry &e, const void*);
extern void dpyCanonImageNumber(OutputContext &ctx, const char *name, const IFDEntry &e, const void*);
extern void dpyCanonSerialNumber(OutputContext &ctx, const char *name, const IFDEntry &e, const void*);

extern void dpyCasioDistance(OutputContext &ctx, const char *name, const IFDEntry &e, const void*);

// Lookup lists

extern lookupValue lookEmpty[];

extern lookupValue lookYesNo[];
extern lookupValue lookPhotometric[];
extern lookupValue lookCompression[];
extern lookupValue lookYCbCrPositioning[];
extern lookupValue lookExposureProgram[];
extern lookupValue lookMeteringMode[];
extern lookupValue lookLightSource[];
extern lookupValue lookSensingMethod[];
extern lookupValue lookOlymJPEGQuality[];
extern lookupValue lookCasioRecMode[];
extern lookupValue lookCasioQuality[];
extern lookupValue lookCasioFocusMode[];
extern lookupValue lookCasioFlashMode[];
extern lookupValue lookCasioFlashInten[];
extern lookupValue lookCasioWhiteBalance[];
extern lookupValue lookCasioDigitalZoom[];
extern lookupValue lookCasioSharpness[];
extern lookupValue lookCasioContrast[];
extern lookupValue lookCasioSaturation[];
extern lookupValue lookCasioSensitivity[];
extern lookupValue lookExifColorSpace[];
extern lookupValue lookExposureMode[];
extern lookupValue lookWhiteBalance[];
extern lookupValue lookSceneCaptureType[];
extern lookupValue lookGainControl[];
extern lookupValue lookContrast[];
extern lookupValue lookSaturation[];
extern lookupValue lookSubjectDistanceRange[];
extern lookupValue lookFocusPosition[];
extern lookupValue lookSceneType[];
extern lookupValue lookFileSource[];
extern lookupValue lookCustomRendered[];

// Tag Format Lists

extern knowntag main_known[];
extern knowntag exif_known[];
extern knowntag empty_known[];

extern knowntag nikon_known[];

extern knowntag olympus_known[];

extern knowntag canon_known[];

extern knowntag casio_known[];

#endif /* DPYFUNCS_H_INCLUDED */
