/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include <iostream>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>

#include "filetiff.h"

static const char *rcsid __attribute__((unused))="$Id: filetiff.cc,v 1.3 2004/08/21 17:40:29 daniel Exp $";

_FileTIFF::_FileTIFF(istream &i, unsigned long ofs)
    : is(i), global_offset(ofs), good(true)
{
    seek(0);
    unsigned char header[2];
    getData(header,2);

    if ((header[0] == 'I') && (header[1] == 'I')) {
	bigendian = false;
    } else if ((header[0] == 'M') && (header[1] == 'M')) {
	bigendian = true;
    } else {
	good = false;
//	cerr << "Unknown byte ordering - aborting" << endl;
//	exit(2);
	return;
    }

    unsigned short checknum = getUShort();

    if (checknum != 42) {
	good = false;
//	cerr << "Bad checknum: " << checknum << endl;
//	exit(2);
	return;
    }
}

_FileTIFF::~_FileTIFF()
{
    // Nothing
}
   
void
_FileTIFF::seek(unsigned long ofs)
{
    is.seekg(ofs+global_offset, ios::beg);
}

size_t
_FileTIFF::getData(unsigned char *buf, size_t bytes)
{
    is.read((char*)buf, bytes);
    if (is.gcount() != bytes) {
//	int err = errno;
//	cerr << "Read failed: " << strerror(err) << endl;
//	exit(2);
	return is.gcount();
    }
    return bytes;
}

unsigned char
_FileTIFF::getUByte()
{
    unsigned char b = 0;
    getData((unsigned char *)&b, 1);
    return b;
}

unsigned short
_FileTIFF::getUShort()
{
    unsigned short s = 0;
    getData((unsigned char *)&s, 2);
    if (bigendian) {return ntohs(s);}
    return s;
}

unsigned long
_FileTIFF::getULong()
{
    unsigned long l = 0;
    getData((unsigned char *)&l, 4);
    if (bigendian) {return ntohl(l);}
    return l;
}

signed char
_FileTIFF::getSByte()
{
    signed char b = 0;
    getData((unsigned char *)&b, 1);
    return b;
}

signed short
_FileTIFF::getSShort()
{
    signed short s = 0;
    getData((unsigned char *)&s, 2);
    if (bigendian) {return ntohs(s);}
    return s;
}

signed long
_FileTIFF::getSLong()
{
    signed long l = 0;
    getData((unsigned char *)&l, 4);
    if (bigendian) {return ntohl(l);}
    return l;
}

IFD
_FileTIFF::getIFD()
{
    return _TIFFDataSource::getIFD(firstIFDOffset());
}

IFD
_FileTIFF::getIFD(unsigned long ofs)
{
    return _TIFFDataSource::getIFD(ofs);
}

bool
_FileTIFF::bigEndian() const
{
    return bigendian;
}

bool
_FileTIFF::isGood() const
{
    return good;
}

