/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include <math.h>

#include "metatiff.h"

static const char *rcsid __attribute__((unused))="$Id: rationals.cc,v 1.4 2002/09/02 19:20:01 daniel Exp $";

static unsigned long Euclid(unsigned long a, unsigned long b)
{
    if (b > a) return Euclid(b, a);
    if (b==0) return a;
    return Euclid(b, a % b);
}


tiffRATIONAL
tiffRATIONAL::normalize() const
{
    if ((num == 0) || (den == 0)) return *this;
    unsigned long d = Euclid(num, den);
    return tiffRATIONAL(num/d, den/d);
}

tiffSRATIONAL
tiffSRATIONAL::normalize() const
{
    if ((num==0) || (den==0)) return *this;
    long d;

    if ((num < 0) && (den<0)) {
	d = Euclid(-num,-den);
    } else if (num<0) {
	d = Euclid(-num,den);
    } else if (den<0) {
	d = Euclid(num,-den);
    } else{
	d = Euclid(num,den);
    }
    return tiffSRATIONAL(num/d, den/d);
}

