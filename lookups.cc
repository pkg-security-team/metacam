/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include "dpyfuncs.h"

static const char *rcsid __attribute__((unused))="$Id: lookups.cc,v 1.3 2002/09/02 19:20:01 daniel Exp $";

lookupValue lookEmpty[] = {
    {0,0}
};

lookupValue lookYesNo[] = {
    {0, "No"},
    {1, "Yes"},
    {0,0}
};

lookupValue lookPhotometric[] = {
    {0, "White Is Zero"},
    {1, "Black Is Zero"},
    {2, "RGB"},
    {3, "RGB Palette"},
    {4, "Transparency Mask"},
    {5, "CMYK"},
    {6, "YCbCr"},
    {8, "CIELab"},
    {0,0}
};

lookupValue lookCompression[] = {
    {1, "Uncompressed"},
    {2, "CCITT 1D"},
    {3, "Group 3 FAX"},
    {4, "Group 4 Fax"},
    {5, "LZW"},
    {6, "JPEG"},
    {32773, "PackBits"},
    {0,0}
};

lookupValue lookYCbCrPositioning[] = {
    {1, "Center of Array"},
    {2, "Datum Point"},
    {0,0}
};

lookupValue lookExposureProgram[] = {
    {0, "Not Defined"},
    {1, "Manual Control"},
    {2, "Program Normal"},
    {3, "Aperture Priority"},
    {4, "Shutter Priority"},
    {5, "Program Creative"},
    {6, "Program Action"},
    {7, "Portrait Mode"},
    {8, "Landscape Mode"},
    {0,0}
};

lookupValue lookMeteringMode[] = {
    {0, "Unknown"},
    {1, "Average"},
    {2, "Center Weighted Average"},
    {3, "Spot"},
    {4, "Multi-Spot"},
    {5, "Multi-Segment/Pattern"},
    {6, "Partial"},
    {255, "Other"},
    {0,0}
};

lookupValue lookLightSource[] = {
    {0, "Automatic"},
    {1, "Daylight"},
    {2, "Fluorescent"},
    {3, "Tungsten"},
    {4, "Flash"},
    {9, "Fine Weather"},
    {10, "Cloudy Weather"},
    {11, "Shade"},
    {12, "Daylight Fluorescent (D 5700-7100K)"},
    {13, "Day White Fluorescent (N 4600-5400K)"},
    {14, "Cool White fluorescent (W 3900-4500K)"},
    {15, "White Fluorescent (WW 3200-3700K)"},
    {17, "Standard Light A"},
    {18, "Standard Light B"},
    {19, "Standard Light C"},
    {20, "D55"}, 
	/* Added 20 - Overcast for Olympus D-510Z
	   Thanks to Geoff Kuenning (geoff@cs.hmc.edu) for patch
	   
	   Updated 2002-09-02 based on Exif 2.2 standard
	*/
    {21, "D65"}, 
    {22, "D75"}, 
    {23, "D50"}, 
    {24, "ISO Studio Tungsten"},
    {255, "Other Light Source"},
    {0,0}
};

lookupValue lookSensingMethod[] = {
    {1, "Not Defined"},
    {2, "Single Chip Color Area Sensor"},
    {3, "Two Chip Color Area Sensor"},
    {4, "Three Chip Color Area Sensor"},
    {5, "Color Sequential Area Sensor"},
    {7, "Trilinear Sensor"},
    {8, "Color Sequential Linear Sensor"},
    {0,0}
};

lookupValue lookOlymJPEGQuality[] = {
    {1, "SQ"},
    {2, "HQ"},
    {3, "SHQ"},
    {0,0}
};

lookupValue lookCasioRecMode[] = {
    {1, "Single Shutter"},
    {2, "Panorama"},
    {3, "Night Scene"},
    {4, "Portrait"},
    {5, "Landscape"},
    {0,0}
};

lookupValue lookCasioQuality[] = {
    {1, "Economy"},
    {2, "Normal"},
    {3, "Fine"},
    {0,0}
};

lookupValue lookCasioFocusMode[] = {
    {2, "Macro"},
    {3, "Auto Focus"},
    {4, "Manual Focus"},
    {5, "Infinity"},
    {0,0}
};

lookupValue lookCasioFlashMode[] = {
    {1, "Auto"},
    {2, "On"},
    {3, "Off"},
    {4, "Red Eye Reduction"},
    {0,0}
};

lookupValue lookCasioFlashInten[] = {
    {11, "Weak"},
    {13, "Normal"},
    {15, "Strong"},
    {0,0}
};

lookupValue lookCasioWhiteBalance[] = {
    {1, "Auto"},
    {2, "Tungsten"},
    {3, "Daylight"},
    {4, "Flurescent"},
    {5, "Shade"},
    {129, "Manual"},
    {0,0}
};

lookupValue lookCasioDigitalZoom[] = {
    {0x10000, "Off"},
    {0x10001, "2X"},
    {0,0}
};

lookupValue lookCasioSharpness[] = {
    {0, "Normal"},
    {1, "Soft"},
    {2, "Hard"},
    {0,0}
};

lookupValue lookCasioContrast[] = {
    {0, "Normal"},
    {1, "Low"},
    {2, "High"},
    {0,0}
};

lookupValue lookCasioSaturation[] = {
    {0, "Normal"},
    {1, "Low"},
    {2, "High"},
    {0,0}
};

lookupValue lookCasioSensitivity[] = {
	    /* QV3000 Only: */
    {64, "Normal"},
    {125, "+1.0"},
    {250, "+2.0"},
    {244, "+3.0"},
	    /* QV2000/QV8000 Only: */
    {80, "Normal"},
    {100, "High"},
    {0,0}
};

lookupValue lookExifColorSpace[] = {
    {1, "sRGB"},
    {0x0FFFF, "Uncalibrated"},
    {0,0}
};

lookupValue lookExposureMode[] = {
    {0, "Auto Exposure"},
    {1, "Manual Exposure"},
    {2, "Auto Bracket"},
    {0,0}
};

lookupValue lookWhiteBalance[] = {
    {0, "Auto White Balance"},
    {1, "Manual White Balance"},
    {0,0}
};

lookupValue lookSceneCaptureType[] = {
    {0, "Standard"},
    {1, "Landscape"},
    {2, "Portrait"},
    {3, "Night Scene"},
    {0,0}
};

lookupValue lookGainControl[] = {
    {0, "None"},
    {1, "Low Gain Up"},
    {2, "High Gain Up"},
    {3, "Low Gain Down"},
    {4, "High Gain Down"},
    {0,0}
};

lookupValue lookContrast[] = {
    {0, "Normal"},
    {1, "Soft"},
    {2, "Hard"},
    {0,0}
};

lookupValue lookSaturation[] = {
    {0, "Normal"},
    {1, "Low Saturation"},
    {2, "High Saturation"},
    {0,0}
};

lookupValue lookSubjectDistanceRange[] = {
    {0, "Unknown"},
    {1, "Macro"},
    {2, "Close View"},
    {3, "Distant View"},
    {0,0}
};

lookupValue lookFocusPosition[] = {
    {0, "Center"},
    {1, "Top"},
    {2, "Bottom"},
    {3, "Left"},
    {4, "Right"},
    {0,0}
};

lookupValue lookCustomRendered[] = {
    {0, "Normal Process"},
    {1, "Custom Process"},
    {0,0}
};

lookupValue lookSceneType[] = {
    {1, "Directly Photographed"},
    {0,0}
};

lookupValue lookFileSource[] = {
    {3, "DSC"},
    {0,0}
};


