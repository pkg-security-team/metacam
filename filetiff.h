/* This file is -*-C++-*-
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------

$Id: filetiff.h,v 1.2 2002/09/02 19:20:01 daniel Exp $

*/

#ifndef FILETIFF_H_INCLUDED
#define FILETIFF_H_INCLUDED

#include <iostream>

#include "metatiff.h"

class _FileTIFF;
// class _FileIFD;
// class _FileIFDEntry;
// class _FileTIFFDataSource;

class _FileTIFF : public _TIFFDataSource
{
    friend class _FileIFD;
    friend class _FileIFDEntry;
    friend class _FileTIFFDataSource;
private:
    istream &is;
    unsigned long global_offset;
    bool bigendian;
    bool good;
protected:
    virtual void seek(unsigned long ofs);
    virtual unsigned long tell() const { return streamoff(is.tellg())-global_offset; }
    virtual size_t getData(unsigned char *buf, size_t bytes);
    virtual unsigned char  getUByte();
    virtual unsigned short getUShort();
    virtual unsigned long  getULong();
    virtual signed char    getSByte();
    virtual signed short   getSShort();
    virtual signed long    getSLong();
    
    virtual bool bigEndian() const;

    unsigned long firstIFDOffset() {
	seek(4);
	return getULong();
    }
public:
    _FileTIFF(istream &i, unsigned long ofs=0);
    virtual ~_FileTIFF();

    virtual IFD getIFD();
    virtual IFD getIFD(unsigned long ofs);

    virtual bool isGood() const;
};

#endif /* FILETIFF_H_INCLUDED */
