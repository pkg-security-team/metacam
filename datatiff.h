/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#ifndef DATATIFF_H_INCLUDED
#define DATATIFF_H_INCLUDED

#include "metatiff.h"

class _DataIFD;
class _DataIFDEntry;

class _DataIFD : public _IFD {
private:
    TIFFDataSource source;

    unsigned long ofs;
    unsigned short entry_count;
    unsigned long next_ifd_ofs;

    _DataIFDEntry **table;

    _DataIFD(const _DataIFD &) { abort(); }
    _DataIFD &operator=(const _DataIFD &) { abort(); return *this; }

public:
    _DataIFD(const TIFFDataSource &src,
	     unsigned long o, unsigned long tagofs = 0) ;
    ~_DataIFD();

    virtual unsigned short entries() const;
    virtual IFD nextIFD() const;
    virtual bool isDynamic() const;

    virtual IFDEntry operator[](int n) const;
    virtual IFDEntry findEntry(unsigned long tag, _IFDEntry *after=0) const;

    virtual TIFFDataSource getSource() const;
    virtual unsigned long getSourceOffset() const;
};


class _DataIFDEntry : public _IFDEntry {
private:
    TIFFDataSource source;

    unsigned long value_count;
    unsigned long offset;
public:
    _DataIFDEntry(const TIFFDataSource &src, long ofs,
		  long t, short typ, long vals);
    virtual ~_DataIFDEntry();

    virtual unsigned long values() const;

    virtual void outputValue(ostream &os) const;

    virtual vector<tiffUNSIGNED>   getUVALUE() const;
    virtual vector<tiffSIGNED>     getSVALUE() const;
    virtual vector<tiffRATIONAL>   getRATIONAL() const;
    virtual vector<tiffSRATIONAL>  getSRATIONAL() const;
    virtual vector<string>         getSTRING() const;
    virtual vector<unsigned char>  getOPAQUE() const;

    virtual TIFFDataSource getSource() const;
    virtual unsigned long getSourceOffset() const;
};


#endif /* DATATIFF_H_INCLUDED */
