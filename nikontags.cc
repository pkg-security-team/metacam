/*
------------------------------------------------------------------------------
MetaCam - Extract EXIF information from digital camera files, with
support for Vendor specific blocks.
Copyright (C) 2000 Daniel Stephens (daniel@cheeseplant.org)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
------------------------------------------------------------------------------
*/

#include "dpyfuncs.h"

static const char *rcsid __attribute__((unused))="$Id: nikontags.cc,v 1.5 2004/08/21 17:54:20 daniel Exp $";

// Nikon tags reverse engineered from a huge collection of well labeled images
// on imaging-resource.com (Great Site BTW)

// A number of additional tags provided by Jeremie Rostand

knowntag nikon_known[] = {
    {0x0001, 7, 0, "Nikon Version Number",     dpyExifVersion, 0},
    {0x0002, 3, 0, "ISO Equivalent",           dpyISO, 0},
    {0x0003, 2, 0, "Color/BW",                 dpyString, 0},
    {0x0004, 2, 0, "File Format",              dpyString, 0},
    {0x0005, 2, 0, "White Balance",            dpyString, 0},
    {0x0006, 2, 0, "Sharpening",               dpyString, 0},
    {0x0007, 2, 0, "Focus",                    dpyString, 0},
    {0x0008, 2, 0, "Flash Mode",               dpyString, 0},
    {0x0009, 2, 0, "Flash Metering mode",      dpyString, 0},
    {0x000b, 8, 0, "White Balance Adjustment", dpySigned, 0},
//  {0x000c, 5, 0, "??? White Point ???", 0, 0},
//  {0x000e, 7, 0, "0x000e               Unknown", dpyExifVersion},
    {0x000f, 2, 0, "ISO Selection",            dpyString, 0},
//  {0x0010, 7, 0, "0x0010               Unknown", dpyExifVersion},
//  {0x0011, 4, 0, "0x0011         Unsigned long", dpyPixels},
    {0x0081, 2, 0, "Image Adjustment",         dpyString, 0},
    {0x0082, 2, 0, "Lens Adapter",             dpyString, 0},
    {0x0084, 5, 0, "Lens",                     dpyLens, 0},
    {0x0085, 5, 0, "Manual Focus Distance",    dpyRationalDistance, "m"},
    {0x0086, 5, 0, "Digital Zoom",             dpyZoomRatio, 0},
    {0x0088, 7, 0, "Focus Position",           dpyNikonFocusPosition, 0},
    {0x0090, 2, 0, "Flash Type",               dpyString, 0},
    {0x0094, 8, 0, "Saturation",               0, 0},
    {0x0095, 2, 0, "Noise Reduction",          dpyString, 0},
    // Added Nikkor Lens Info (Thanks to rr for providing details)
    {0x0098, 7, 0, "Nikkor Lens Info",         dpyNikkorLensInfo, 0},
    
    {0,0,0,0,0}
};
